package com.company;

import java.util.ArrayList;

public class Maquina {
    private ArrayList<Slot> slots = new ArrayList<>();

    public Maquina() {
        this.slots.add(new Slot());
        this.slots.add(new Slot());
        this.slots.add(new Slot());
    }

    public void adicionarSlot(Slot slot) {
        this.slots.add(slot);
    }

    public ArrayList<Slot> getSlots() {
        return slots;
    }
}
