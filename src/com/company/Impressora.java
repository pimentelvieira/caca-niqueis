package com.company;

import java.util.ArrayList;

public class Impressora {
    public static void imprimir(ArrayList<Opcao> opcoes, int total) {
        for (Opcao opcao : opcoes) {
            System.out.println(opcao.getNome() + " - " + opcao.getPontuacao());
        }
        System.out.println();
        System.out.println("Total - " + total);
    }
}
