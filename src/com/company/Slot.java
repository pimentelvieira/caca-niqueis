package com.company;

import java.util.ArrayList;

public class Slot {

    private ArrayList<Opcao> opcoes = new ArrayList<>();

    public Slot() {
        this.opcoes.add(new Opcao("Banana", 10));
        this.opcoes.add(new Opcao("Framboesa", 50));
        this.opcoes.add(new Opcao("Moeda", 100));
        this.opcoes.add(new Opcao("Sete", 300));
    }

    public void adicionarOpcao(Opcao opcao) {
        this.opcoes.add(opcao);
    }

    public ArrayList<Opcao> getOpcoes() {
        return opcoes;
    }
}
