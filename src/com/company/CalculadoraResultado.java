package com.company;

import java.util.ArrayList;

public class CalculadoraResultado {

    public int calcular(ArrayList<Opcao> opcoes) {
        int soma = 0;

        for (int i = 0; i < opcoes.size(); i++) {
            soma += opcoes.get(i).getPontuacao();
        }

        return todosIguais(opcoes) ? soma * 100 : soma;
    }

    private boolean todosIguais(ArrayList<Opcao> opcoes) {
        boolean result = true;

        for(Opcao opcao : opcoes) {
            if (!opcao.getNome().equals(opcoes.get(0).getNome())) {
                result = false;
            }
        }

        return result;
    }
}
