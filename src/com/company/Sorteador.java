package com.company;

import java.util.ArrayList;

public class Sorteador {

    private Maquina maquina;
    private ArrayList<Opcao> opcoesSorteadas = new ArrayList<>();

    public Sorteador(Maquina maquina) {
        this.maquina = maquina;
    }

    public void sortear() {
        for (int i = 0; i < this.maquina.getSlots().size(); i++) {
            opcoesSorteadas.add(this.maquina.getSlots().get(i).getOpcoes().get(Randomizador.getInt()));
        }
    }

    public ArrayList<Opcao> getOpcoesSorteadas() {
        return opcoesSorteadas;
    }
}
