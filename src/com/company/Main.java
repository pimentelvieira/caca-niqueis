package com.company;

public class Main {

    public static void main(String[] args) {
        Sorteador sorteador = new Sorteador(new Maquina());
        sorteador.sortear();
        Impressora.imprimir(sorteador.getOpcoesSorteadas(), new CalculadoraResultado().calcular(sorteador.getOpcoesSorteadas()));
    }
}
