package com.company;

public class Opcao {

    private String nome;
    private int pontuacao;

    public Opcao(String nome, int pontuacao) {
        this.nome = nome;
        this.pontuacao = pontuacao;
    }

    public String getNome() {
        return nome;
    }

    public int getPontuacao() {
        return pontuacao;
    }
}
